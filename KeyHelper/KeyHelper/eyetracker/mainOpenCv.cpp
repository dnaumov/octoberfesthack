#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>

#include "findEyeCentre.h"
#include "findEyeCorner.h"


using namespace std;
using namespace cv;

static const int width = 640;
static const int height = 480;


void findEyes(Mat frame_gray, Mat normalImage, cv::Rect face, Mat outputFrame);

/** Function Headers */
void detectAndDisplay( Mat frame );

/** Global variables */
String face_cascade_name = "haarcascade_frontalface_default.xml";
String eyes_cascade_name = "haarcascade_eye_tree_eyeglasses.xml";
CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;
string window_name = "Capture - Face detection";
RNG rng(12345);

/** @function main */
int main( int argc, const char** argv )
{
    CvCapture* capture;
    Mat frame;
    
    //-- 1. Load the cascades
    if( !face_cascade.load( face_cascade_name ) ){ printf("--(!)Error loading\n"); return -1; };
    if( !eyes_cascade.load( eyes_cascade_name ) ){ printf("--(!)Error loading\n"); return -1; };
    
    //-- 2. Read the video stream
    capture = cvCaptureFromCAM( -1 );
    cvSetCaptureProperty( capture, CV_CAP_PROP_FRAME_WIDTH, width );
    cvSetCaptureProperty( capture, CV_CAP_PROP_FRAME_HEIGHT, height );
    if( capture )
        {
        while( true )
            {
            IplImage *image = cvQueryFrame( capture );
            frame = cv::cvarrToMat(image);
            
            //-- 3. Apply the classifier to the frame
            if( !frame.empty() )
                { detectAndDisplay( frame ); }
            else
                { printf(" --(!) No captured frame -- Break!"); break; }
            
            int c = waitKey(10);
            if( (char)c == 'c' ) { break; }
            }
        }
    return 0;
}

/** @function detectAndDisplay */
void detectAndDisplay( Mat frame )
{
    std::vector<Rect> faces;
    Mat frame_gray;
    
    cvtColor( frame, frame_gray, CV_BGR2GRAY );
    equalizeHist( frame_gray, frame_gray );
    
    //-- Detect faces
    face_cascade.detectMultiScale( frame_gray, faces, 1.2, 2, 0|CV_HAAR_DO_CANNY_PRUNING, Size((float)width*0.3, (float)height*0.185));
    
    for( size_t i = 0; i < faces.size(); i++ )
        {
        Point center( faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5 );
        rectangle(frame, faces[i].tl(), faces[i].br(), Scalar( 255, 0, 255 ));
        Mat faceROI = frame_gray( faces[i] );
        std::vector<Rect> eyes;
        
        Mat imageToDraw;
        findEyes(frame_gray, frame, faces[i], frame);
        }
    //-- Show what you got
    imshow( window_name, frame );
}

float top = 0.001;

int framesAtPosition = 0;
bool eyesAreTop = 0;
void findEyes(Mat frame_gray, Mat normalImage, cv::Rect face, Mat outputFrame) {
    cv::Mat faceROI = frame_gray(face);
    cv::Mat normalFace = normalImage(face);
    //cv::Mat debugFace;
    
    GaussianBlur( faceROI, faceROI, cv::Size( 0, 0 ), 0.1);
    
    //-- Find eye regions and draw them
    int eye_region_width = face.width * (25/100.0);
    int eye_region_height = face.width * (20/100.0);
    int eye_region_top = face.height * (28 /100.0);
//    int eye_region_top = face.height * (31/100.0);
    cv::Rect leftEyeRegion(face.width*(55/100.0),
                           eye_region_top,eye_region_width,eye_region_height);
    cv::Rect rightEyeRegion(face.width - eye_region_width - face.width*(55/100.0),
                            eye_region_top,eye_region_width,eye_region_height);
    
    //-- Find Eye Centers
    
    cv::Point leftPupil = findEyeCentre(faceROI,leftEyeRegion,outputFrame);
    cv::Point rightPupil = findEyeCentre(faceROI, rightEyeRegion, outputFrame);
    // get corner regions
    cv::Rect leftRightCornerRegion(leftEyeRegion);
    leftRightCornerRegion.width -= leftPupil.x;
    leftRightCornerRegion.x += leftPupil.x;
    leftRightCornerRegion.height /= 2;
    leftRightCornerRegion.y += leftRightCornerRegion.height / 2;
    cv::Rect leftLeftCornerRegion(leftEyeRegion);
    leftLeftCornerRegion.width = leftPupil.x;
    leftLeftCornerRegion.height /= 2;
    leftLeftCornerRegion.y += leftLeftCornerRegion.height / 2;
    cv::Rect rightLeftCornerRegion(rightEyeRegion);
    rightLeftCornerRegion.width = rightPupil.x;
    rightLeftCornerRegion.height /= 2;
    rightLeftCornerRegion.y += rightLeftCornerRegion.height / 2;
    cv::Rect rightRightCornerRegion(rightEyeRegion);
    rightRightCornerRegion.width -= rightPupil.x;
    rightRightCornerRegion.x += rightPupil.x;
    rightRightCornerRegion.height /= 2;
    rightRightCornerRegion.y += rightRightCornerRegion.height / 2;
    rectangle(normalFace,leftRightCornerRegion,Scalar( 255, 49, 255 ));
    rectangle(normalFace,leftLeftCornerRegion,Scalar( 255, 49, 255 ));
    rectangle(normalFace,rightLeftCornerRegion,Scalar( 255, 49, 255 ));
    rectangle(normalFace,rightRightCornerRegion,Scalar( 255, 49, 255 ));
    rightPupil.x += rightEyeRegion.x;
    rightPupil.y += rightEyeRegion.y;
    leftPupil.x += leftEyeRegion.x;
    leftPupil.y += leftEyeRegion.y;
    // draw eye centers
    circle(normalFace, rightPupil, 2, Scalar( 0, 190, 40 ));
    circle(normalFace, leftPupil, 2, Scalar( 0, 190, 40 ));
    bool enableCorners = 1;
    if (enableCorners) {
        cv::Point2f leftRightCorner = findEyeCorner(faceROI(leftRightCornerRegion), true, false);
        leftRightCorner.x += leftRightCornerRegion.x;
        leftRightCorner.y += leftRightCornerRegion.y;
        cv::Point2f leftLeftCorner = findEyeCorner(faceROI(leftLeftCornerRegion),true, true);
        leftLeftCorner.x += leftLeftCornerRegion.x;
        leftLeftCorner.y += leftLeftCornerRegion.y;
        cv::Point2f rightLeftCorner = findEyeCorner(faceROI(rightLeftCornerRegion),false, true);
        rightLeftCorner.x += rightLeftCornerRegion.x;
        rightLeftCorner.y += rightLeftCornerRegion.y;
        cv::Point2f rightRightCorner = findEyeCorner(faceROI(rightRightCornerRegion), false,false);
        rightRightCorner.x += rightRightCornerRegion.x;
        rightRightCorner.y += rightRightCornerRegion.y;
        circle(normalFace, leftRightCorner, 1, Scalar( 255, 49, 255 ));
        circle(normalFace, leftLeftCorner, 1, Scalar( 0, 0, 255 ));
        circle(normalFace, rightLeftCorner, 1, Scalar( 255, 49, 255 ));
        circle(normalFace, rightRightCorner, 1, Scalar( 0, 0, 255 ));
        
        bool rightDown = (((rightPupil.y - rightLeftCorner.y) + (rightPupil.y - rightRightCorner.y)) > 0);
        bool leftDown = (((leftPupil.y - leftLeftCorner.y) + (leftPupil.y - leftRightCorner.y)) > 0);
        
        if (rightDown || leftDown){
            framesAtPosition++;
            if (framesAtPosition > 7)
                {
                    printf("DOWN!!!");
                    circle(outputFrame, cvPoint(20, 20), 10, Scalar( 0, 0, 255 ),10, 10, 0 );
                }
        }
        else {
            framesAtPosition = 0;
            printf("-");
        }
    }
    normalFace.copyTo(outputFrame);
}