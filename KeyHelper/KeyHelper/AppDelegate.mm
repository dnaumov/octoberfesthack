//
//  AppDelegate.m
//  KeyHelper
//
//  Created by Dmytro Naumov on 26/09/15.
//  Copyright © 2015 Dima Home. All rights reserved.
//

#import "AppDelegate.h"
#import "mainOpenCv.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    startTrack();
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
