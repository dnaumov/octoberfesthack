//
//  StartWindow.m
//  KeyHelper
//
//  Created by Dmytro Naumov on 26/09/15.
//  Copyright © 2015 Dima Home. All rights reserved.
//

#import "StartWindow.h"
#import <Carbon/carbon.h>
@interface StartWindow()
{
    int rowFireCount;
    float alpha;
}

@end

@implementation StartWindow

- (id)initWithContentRect:(NSRect)contentRect styleMask:(NSUInteger)aStyle backing:(NSBackingStoreType)bufferingType defer:(BOOL)flag
{
    self = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask backing:bufferingType defer:flag];
    
    if ( self ) {
        [self setOpaque:NO];
        [self setHasShadow:NO];
        [self setLevel:NSFloatingWindowLevel];
        [self setBackgroundColor:[NSColor clearColor]];
        [self setAlphaValue:1.0];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eyesGoingDown) name:@"eyesDown" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearOverlay) name:@"eyesNormal" object:nil];
    
    return self;
}

-(void)eyesGoingDown{
    rowFireCount ++;
    alpha = (alpha < 1.0) ? alpha + 0.1 :1.0;
    [self setOverlay];
}

- (void) clearOverlay{
    rowFireCount = 0;
    alpha = (alpha> 0) ? alpha - 0.1 : 0;
    [self setOverlay];
}

- (void) setOverlay{
    [trackingWin setBackgroundColor:[NSColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:alpha]];
}

- (void)awakeFromNib
{
    trackingWin=[[NSWindow alloc]
                 initWithContentRect:[self screenSize]
                 styleMask:NSBorderlessWindowMask
                 backing:NSBackingStoreBuffered
                 defer:YES];
    [trackingWin setOpaque:NO];
    [trackingWin setAlphaValue:0.5];
    [trackingWin setBackgroundColor:[NSColor redColor]];
    [trackingWin setHasShadow:YES];
    [trackingWin setLevel:NSFloatingWindowLevel];
    

    [trackingWin setIgnoresMouseEvents:YES];
    [trackingWin orderFront:self];
    
    [self addChildWindow:trackingWin ordered:NSWindowAbove];
    
    // Tracking areas need to be setup to handle when the mouse moves into or out of the main window
//    EventTypeSpec eventType;
//    
//    gAppHotKeyFunction = NewEventHandlerUPP(MyHotKeyHandler);
//    eventType.eventClass=kEventClassKeyboard;
//    eventType.eventKind=kEventHotKeyPressed;
//    InstallApplicationEventHandler(gAppHotKeyFunction,1,&eventType,self,NULL);
//    gMyHotKeyID.signature=kMyHotKeyIdentifier;
//    gMyHotKeyID.id=1;
//    
//    RegisterEventHotKey(kMyHotKey, cmdKey, gMyHotKeyID, GetApplicationEventTarget(), 0, &gMyHotKeyRef);

}

- (void)keyDown:(NSEvent *)theEvent {
    [self interpretKeyEvents:[NSArray arrayWithObject:theEvent]];
}

- (BOOL)canBecomeKeyWindow
{
    return YES;
}

- (NSRect)screenSize{
    NSRect screenRect;
    NSArray *screenArray = [NSScreen screens];
    NSUInteger screenCount = [screenArray count];
    NSUInteger index  = 0;
    
    for (; index < screenCount; index++)
    {
        NSScreen *screen = [screenArray objectAtIndex: index];
        screenRect = [screen visibleFrame];
    }
    return screenRect;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
