//
//  main.m
//  KeyHelper
//
//  Created by Dmytro Naumov on 26/09/15.
//  Copyright © 2015 Dima Home. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
